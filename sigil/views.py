from django.http import HttpResponse

from sigil import __version__


def project_metadata(request):
    return HttpResponse(f'Sigil API {__version__}')
